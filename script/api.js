const path = "https://restcountries.eu/rest/v2"
const URL1 = `${path}/all`;
const URL2 = `${path}/name`;

const apiFetch = async(URL, name = "") => {
    try {
        const result = await fetch(`${URL}/${name}`);
        const countries = await result.json();
        return countries;
    } catch(error) {
        console.error(error);
    }
}

export const getCountries = async() =>{
    return apiFetch(URL1);
}
export const getCountry = async(name) =>{
    return apiFetch(URL2, name);
}
