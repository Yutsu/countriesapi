import { getCountry } from "./api.js";
import { checkThemeLoad, clickModeTheme } from "./modeTheme.js";
import { getClass } from "./getElement.js";

const mode = getClass('.mode');
const container = getClass('.country');
const nameCountry = location.search.substring(1);
const theme = window.localStorage.currentTheme;


const point = (item) => {
    return item.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}
const check = (item) => {
    if(item.length == 0) return item += "X";
    return item;
}
const mapping = (item) => {
    let info = item.map(index => { return index; });
    return check(info.join(", "));
}
const deepMapping = (item) => {
    let info = item.map(index => { return index.name; });
    return check(info.join(", "));
}

const displayCountry = async(nameCountry) => {
    let country = await getCountry(nameCountry);
    country.map(info => {
        const { name, flag, capital, area, population, nativeName, subregion } = info;
        const { borders, languages, regionalBlocs, translations } = info;
        const div = document.createElement('div');
        div.classList.add('card');
        const table = document.createElement('table');
        table.classList.add('center')
        const translate = document.createElement('td');
        const nameTranslated = document.createElement('td');

        div.innerHTML = `
            <h1>${name}</h1>
            <img src="${flag}" alt="${name}">
            <div class="country-info">
                <span><strong>Capital : </strong> ${check(capital)}</span>
                <span><strong>Population : </strong> ${check(point(population))}</span>
                <span><strong>Native name : </strong> ${check(nativeName)}</span>
                <span><strong>Areas : </strong> ${check(point(area))}</span>
                <span><strong>Subregion : </strong> ${check(subregion)}</span>
                <span><strong>Borders : </strong> ${mapping(borders)} </span>
                <span><strong>Languages : </strong> ${deepMapping(languages)}</span>
                <span><strong>Regional blocs : </strong> ${deepMapping(regionalBlocs)}</span>
            </div>
        `

        table.innerHTML = "<th>Translate</th><th>Name</th>";
        const transLength = Object.keys(translations).length;
        for (let i = 0; i < transLength; i++){
            translate.innerHTML += `<td>${Object.keys(translations)[i]}</td><br>`;
        }
        Object.keys(translations).forEach( (key) => {
            nameTranslated.innerHTML += `<td>${(translations[key])}</td><br>`;
        })

        table.appendChild(translate);
        table.appendChild(nameTranslated);
        div.appendChild(table);
        container.appendChild(div);
    });
}


const app = async() => {
    await window.addEventListener('DOMContentLoaded', checkThemeLoad(theme));
    mode.addEventListener('click', clickModeTheme);
    displayCountry(nameCountry);
}
app();
