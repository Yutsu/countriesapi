import { getCountries } from "./api.js";
import { checkThemeLoad, clickModeTheme } from "./modeTheme.js";
import { getClass, getId } from "./getElement.js";

const theme = window.localStorage.currentTheme;
const mode = getClass('.mode');
const container = getClass('.countries');
const filterRegion = getId('filter');
const filterList = getClass('.filter-list');
const notFound = getClass('.notFound');
const filterRegionName = getClass('.filterByRegion');
const search = getId('input');


const searchCountry = async(e) => {
    const item = e.target.value.trim().toLowerCase();
    let countries = await getCountries();
    let searchCountries = countries.filter(country => {
        return country.name.toLowerCase().includes(item);
    });
    searchCountries.length == 0 ? notFound.style.display="flex" : notFound.style.display="none";
    displayCountries(searchCountries);
}

const filter = async() => {
    filterList.classList.toggle('remove');
    let countries = await getCountries();
    let regions = countries.map(country => {
        return `
            <li class="region">${country.region}</li>
        `
    });
    let uniq = [...new Set(regions)];
    uniq.unshift(`<li class="region">All</li>`);
    filterList.innerHTML = uniq.join("");

    const allRegions = document.querySelectorAll('.region');
    allRegions.forEach(region => {
        region.addEventListener('click', async() => {
            let regionName = region.textContent;
            let changeName = filterRegionName.firstChild;

            if(input.value) input.value = "";
            if(regionName !== "All"){
                let name = countries.filter(country => {
                    return country.region.includes(regionName);
                });
                changeName.textContent = regionName;
                displayCountries(name);
            } else {
                changeName.textContent= "Filter by Region";
                contentLoadedNow();
            }
        });
    })
}

const contentLoadedNow = async() => {
    let countries = await getCountries();
    displayCountries(countries);
}

const displayCountries = async(countries) => {
    let country = countries.map(country => {
    const { flag, name, population, region, capital } = country;
    const pop = population.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

    return `
        <div class="card">
            <a href="./country.html?${name}">
                <img src="${flag}" alt="${name}">
            </a>
            <div class="card-info">
                <h1 class="card-name">${name}</h1>
                <span><strong>Population : </strong>${pop}</span>
                <span><strong>Région : </strong>${region}</span>
                <span><strong>Capital : </strong>${capital}</span>
            </div>
        </div>
    `
    }).join("");
    container.innerHTML = country;
}


const app = async() => {
    await window.addEventListener('DOMContentLoaded', checkThemeLoad(theme));
    await window.addEventListener('DOMContentLoaded', contentLoadedNow);
    mode.addEventListener('click', clickModeTheme);
    search.addEventListener('input', searchCountry);
    filterRegion.addEventListener('click', filter);
}
app();
