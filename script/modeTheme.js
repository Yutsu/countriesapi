import { getClass } from "./getElement.js";

const body = getClass('body');
const darkMode = getClass('.darkMode');
const lightMode = getClass('.lightMode');

export const clickModeTheme = () => {
    const theme = window.localStorage.currentTheme;
    body.classList.add(theme);

    //click then
    if(body.classList.contains("light")){
        body.classList.add('dark');
        body.classList.remove('light');

        localStorage.removeItem('currentTheme');
        localStorage.currentTheme = "dark";

        lightMode.classList.add('hide');
        darkMode.classList.remove('hide');
    }
    else {
        body.classList.add('light');
        body.classList.remove('dark');

        localStorage.removeItem('currentTheme');
        localStorage.currentTheme = "light";

        lightMode.classList.remove('hide');
        darkMode.classList.add('hide');
    }
}

export const checkThemeLoad = async(theme) => {
    body.classList.add(theme);
    if(body.classList.contains("light")){
        body.classList.add('light');
        body.classList.remove('dark');

        lightMode.classList.remove('hide');
        darkMode.classList.add('hide');
    }
    else {
        body.classList.remove('light');
        body.classList.add('dark');

        lightMode.classList.add('hide');
        darkMode.classList.remove('hide');
    }
}